/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Controller, Get, Post, Put, Delete, Body, Param, HttpStatus } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersDTO } from './users.dto';
import { ApiBody, ApiCreatedResponse } from '@nestjs/swagger';

@Controller('user')
export class UsersController {
  constructor(private usersService: UsersService) { }

  @Get()
  async showAllUsers() {
    return {
      statusCode: HttpStatus.OK,
      data: await this.usersService.showAll(),
    };
  }

  
  @Post()
  @ApiCreatedResponse({ description: 'User creation' })
  @ApiBody({ type: UsersDTO })
  async createUsers(@Body() data: UsersDTO) {
    return {
      statusCode: HttpStatus.OK,
      message: 'User added successfully',
      data: await this.usersService.create(data),
    };
  }

  @Get(':id')
  @ApiCreatedResponse({ description: 'Get by user' })
  async readUser(@Param('id') id: number) {
    return {
      statusCode: HttpStatus.OK,
      data: await this.usersService.read(id),
    };
  }

  //   @Get(':firstName')
  //   async readUserByFirstName(@Param('firstName') firstName: string) {
  //     return {
  //       statusCode: HttpStatus.OK,
  //       data: await this.usersService.findByfirstName(firstName),
  //     };
  //   }

  @Put(':id')
  async uppdateUser(@Param('id') id: number, @Body() data: Partial<UsersDTO>) {
    return {
      statusCode: HttpStatus.OK,
      message: 'User update successfully',
      data: await this.usersService.update(id, data),
    };
  }

  @Delete(':id')
  async deleteUser(@Param('id') id: number) {
    await this.usersService.destroy(id);
    return {
      statusCode: HttpStatus.OK,
      message: 'User deleted successfully',
    };
  }
}
