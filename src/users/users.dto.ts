import { ApiProperty } from "@nestjs/swagger";

export class UsersDTO {
  @ApiProperty({ type: Number, description: "id" })
  id: number;

  @ApiProperty({ type: String, description: "Firstname" })
  firstName: string;

  @ApiProperty({ type: String, description: "lastName" })
  lastName: string;

  @ApiProperty({ type: String, description: "email" })
  email: string;

  @ApiProperty({ type: String, description: "address" })
  address: string;

  @ApiProperty({ type: Number, description: "phoneNumber" })
  phoneNumber: number;
}